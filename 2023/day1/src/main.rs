use std::io::Result;
use util;

fn get_calibration_val_for_line(line: &String) -> usize {
    let mut numbers = Vec::new();

    let new_line = line
        .replace("one", "o1e")
        .replace("two", "t2o")
        .replace("three", "t3e")
        .replace("four", "f4r")
        .replace("five", "f5e")
        .replace("six", "s6x")
        .replace("seven", "s7n")
        .replace("eight", "e8t")
        .replace("nine", "n9e");

    for c in new_line.chars() {
        if c.is_numeric() {
            numbers.push(c);
        }
    }

    let first_last = vec![&numbers[0], numbers.last().unwrap()];
    first_last
        .into_iter()
        .collect::<String>()
        .parse::<usize>()
        .unwrap()
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines = util::read_file_to_vec_from_path(file_path)?;

    let total: usize = lines.iter().map(|l| get_calibration_val_for_line(l)).sum();

    println!("Total: {}", total);

    Ok(())
}
