use std::io::Result;
use std::str::FromStr;
use util;

#[derive(Debug)]
struct Race {
    time: isize,
    distance: isize,
}

impl Race {
    fn find_all_winning_times(&self) -> Vec<isize> {
        let mut results = Vec::new();
        // speed = time_held
        // distance = (total_time - speed) * speed
        for i in 1..self.time {
            if (self.time - i) * i > self.distance {
                results.push(i);
            }
        }

        results
    }
}

fn parse_line_to_vec<T>(line: &String) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Debug,
{
    line.split_whitespace().collect::<Vec<&str>>()[1..]
        .into_iter()
        .map(|l| l.parse::<T>().unwrap())
        .collect::<Vec<T>>()
}

fn parse_races_from_file_part_one(lines: Vec<String>) -> Vec<Race> {
    let time_cols = parse_line_to_vec(&lines[0]);
    let dist_cols = parse_line_to_vec(&lines[1]);

    time_cols
        .into_iter()
        .zip(dist_cols.into_iter())
        .map(|t| Race {
            time: t.0,
            distance: t.1,
        })
        .collect::<Vec<Race>>()
}

fn part_one(lines: Vec<String>) {
    let races = parse_races_from_file_part_one(lines);

    let answer: usize = races
        .into_iter()
        .map(|r| r.find_all_winning_times().len())
        .product();

    println!("Part one: {:#?}", answer);
}

fn parse_races_from_file_part_two(lines: Vec<String>) -> Race {
    let time_cols = parse_line_to_vec::<String>(&lines[0]);
    let dist_cols = parse_line_to_vec::<String>(&lines[1]);

    Race {
        time: time_cols.join("").parse::<isize>().unwrap(),
        distance: dist_cols.join("").parse::<isize>().unwrap(),
    }
}

fn part_two(lines: Vec<String>) {
    let race = parse_races_from_file_part_two(lines);

    let answer = race.find_all_winning_times().len();
    println!("Part two: {:#?}", answer);
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines: Vec<String> = util::read_file_to_vec_from_path(file_path)?;
    part_one(lines.clone());

    part_two(lines);
    Ok(())
}
