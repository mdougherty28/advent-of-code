use std::collections::HashMap;

use util;

#[derive(Debug)]
struct Color {
    name: String,
    count: usize,
}

type Round = Vec<Color>;

#[derive(Debug)]
struct Game {
    id: usize,
    rounds: Vec<Round>,
    is_possible_with_bag: bool,
    min_cubes_required: HashMap<String, usize>,
}

fn parse_file_to_games(lines: Vec<String>, bag: HashMap<String, usize>) -> Vec<Game> {
    let mut games = Vec::new();

    for line in lines {
        let mut is_possible_with_bag = true;
        let split_line: Vec<&str> = line.split(":").collect();
        let id = split_line[0].split(" ").collect::<Vec<&str>>()[1]
            .parse::<usize>()
            .unwrap();

        let mut rounds: Vec<Round> = Vec::new();
        let mut max_colors_for_game: HashMap<String, usize> = HashMap::from([
            ("red".to_string(), 0),
            ("green".to_string(), 0),
            ("blue".to_string(), 0),
        ]);

        split_line[1].trim().split(";").for_each(|f| {
            let mut round = Round::new();
            // each round

            f.trim().split(", ").for_each(|r| {
                // each color in each round
                let split_cubes: Vec<&str> = r.split(" ").collect();

                let color = Color {
                    name: String::from(split_cubes[1]),
                    count: split_cubes[0].parse::<usize>().unwrap(),
                };

                // part one
                // if &color.count > bag.get(&color.name).unwrap() {
                // is_possible_with_bag = false;
                // return;
                // }

                // round two
                let max_value = max_colors_for_game.get_mut(&color.name).unwrap();
                if &color.count > max_value {
                    *max_value = color.count;
                }

                round.push(color);
            });

            rounds.push(round);
        });

        max_colors_for_game.insert(
            "product_of_values".to_string(),
            max_colors_for_game.values().product(),
        );
        games.push(Game {
            id: id,
            rounds: rounds,
            is_possible_with_bag: is_possible_with_bag,
            min_cubes_required: max_colors_for_game,
        });
    }

    games
}

fn main() -> std::io::Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines = util::read_file_to_vec_from_path(file_path)?;

    // part one
    let bag: HashMap<String, usize> = HashMap::from([
        ("red".to_string(), 12),
        ("green".to_string(), 13),
        ("blue".to_string(), 14),
    ]);

    let games_played = parse_file_to_games(lines, bag);

    // part one
    // let sum_ids_of_possible_games: usize = games_played
    //     .iter()
    //     .map(|g| if g.is_possible_with_bag { g.id } else { 0 })
    //     .sum();
    // println!("{:#?}", sum_ids_of_possible_games);

    // part two
    let total: usize = games_played
        .iter()
        .map(|g| g.min_cubes_required.get("product_of_values").unwrap())
        .sum();
    println!("Sum of the power of possible sets of cubes: {}", total);

    Ok(())
}
