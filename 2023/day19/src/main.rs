use core::panic;
use regex::Regex;
use std::io::Result;
use util;

#[derive(Debug)]
struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Condition {
    category: String,
    comparitor: String,
    value: usize,
}

#[derive(Debug, Clone)]
struct Rule {
    condition: Condition,
    dest: String,
}

#[derive(Debug, Clone)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
    default: String,
}

fn parse_lines(lines: Vec<String>) -> (Vec<Workflow>, Vec<Part>) {
    let mut workflows = Vec::new();
    let mut parts = Vec::new();
    let mut done_parsing_workflows = false;

    for (idx, line) in lines.iter().enumerate() {
        if done_parsing_workflows {
            let re =
                Regex::new(r"\{[x]=([0-9]+),[m]=([0-9]+),[a]=([0-9]+),[s]=([0-9]+)\}").unwrap();

            let Some(captures) = re.captures(line) else {
                continue;
            };

            parts.push(Part {
                x: captures[1].parse::<usize>().unwrap(),
                m: captures[2].parse::<usize>().unwrap(),
                a: captures[3].parse::<usize>().unwrap(),
                s: captures[4].parse::<usize>().unwrap(),
            })
        } else {
            if line.len() == 0 {
                done_parsing_workflows = true;
            } else {
                let rule_capture_group = r"([a-z])([<>])([0-9]+):([aA-zZ]+),";
                let mut regex_str = String::from(r"([a-z]+)\{");
                for _ in 0..line.chars().filter(|c| c == &',').count() {
                    regex_str += rule_capture_group;
                }
                regex_str += r"([a-zA-Z]+)\}";
                let re = Regex::new(&regex_str).unwrap();
                let Some(captures) = re.captures(line) else {
                    println!("Didn't find any matches.");
                    continue;
                };

                let mut wf = Workflow {
                    name: captures[1].to_string(),
                    rules: Vec::new(),
                    default: captures[captures.len() - 1].to_string(),
                };

                for i in (2..captures.len() - 1).step_by(4) {
                    wf.rules.push(Rule {
                        condition: Condition {
                            category: captures[i].to_string(),
                            comparitor: captures[i + 1].to_string(),
                            value: captures[i + 2].parse::<usize>().unwrap(),
                        },
                        dest: captures[i + 3].to_string(),
                    });
                }

                workflows.push(wf);
            }
        }
    }

    (workflows, parts)
}

fn get_workflow_by_name(name: &str, workflows: &Vec<Workflow>) -> Workflow {
    for w in workflows {
        if w.name == name {
            return w.clone();
        }
    }
    workflows.iter().find(|w| w.name == name).unwrap().clone()
}

fn sort_part(part: &Part, workflows: &Vec<Workflow>) -> String {
    let mut current_workflow = "in".to_string();

    'outer: loop {
        if Vec::from(["A".to_string(), "R".to_string()]).contains(&current_workflow) {
            return current_workflow;
        }
        let workflow = get_workflow_by_name(&current_workflow, workflows);

        for rule in workflow.rules {
            match rule.condition.category.as_str() {
                "x" => {
                    if rule.condition.comparitor == ">" {
                        if part.x > rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    } else {
                        if part.x < rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    }
                }
                "m" => {
                    if rule.condition.comparitor == ">" {
                        if part.m > rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    } else {
                        if part.m < rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    }
                }
                "a" => {
                    if rule.condition.comparitor == ">" {
                        if part.a > rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    } else {
                        if part.a < rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    }
                }
                "s" => {
                    if rule.condition.comparitor == ">" {
                        if part.s > rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    } else {
                        if part.s < rule.condition.value {
                            current_workflow = rule.dest;
                            continue 'outer;
                        }
                    }
                }
                _ => {
                    panic!("no match found.")
                }
            }
        }
        current_workflow = workflow.default
    }
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines = util::read_file_to_vec_from_path(file_path)?;

    let (workflows, parts) = parse_lines(lines);
    part_one(&workflows, parts);

    Ok(())
}

fn part_one(workflows: &Vec<Workflow>, parts: Vec<Part>) {
    let mut total = 0;
    for p in parts {
        let r = sort_part(&p, &workflows);

        if r == "A" {
            total += p.x + p.a + p.m + p.s;
        }
    }
    println!("Part one:{:#?}", total);
}
