use std::fs::File;
use std::io::Result;
use std::io::{BufRead, BufReader};
use std::ops::Range;

#[derive(Debug, Clone)]
struct SourceDestMapping {
    source: String,
    dest: String,
    mapping_ranges: Vec<MappingRange>,
}

impl SourceDestMapping {
    fn new() -> Self {
        SourceDestMapping {
            source: String::new(),
            dest: String::new(),
            mapping_ranges: Vec::new(),
        }
    }
}

#[derive(Debug, Clone)]
struct MappingRange {
    source_range: Range<usize>,
    dest_range: Range<usize>,
}

impl MappingRange {
    fn new(source_range_start: usize, dest_range_start: usize, range_length: usize) -> Self {
        MappingRange {
            source_range: source_range_start..source_range_start + range_length,
            dest_range: dest_range_start..dest_range_start + range_length,
        }
    }
}

// fn parse_seeds_line_part_one(line: &String) -> Vec<usize> {
//     line.split("seeds: ").collect::<Vec<&str>>()[1]
//         .split_whitespace()
//         .map(|n| {
//             n.parse::<usize>()
//                 .expect("Failed parsing seed number string")
//         })
//         .collect::<Vec<usize>>()
// }

fn parse_seeds_line_part_two(line: &String) -> Vec<Range<usize>> {
    let seeds_vec = line.split("seeds: ").collect::<Vec<&str>>()[1]
        .split_whitespace()
        .map(|n| {
            n.parse::<usize>()
                .expect("Failed parsing seed number string")
        })
        .collect::<Vec<usize>>();

    let mut result = Vec::new();
    for i in (0..seeds_vec.len() - 1).step_by(2) {
        let range_start = seeds_vec[i];
        let range_end = range_start + seeds_vec[i + 1];
        result.push(range_start..range_end);
    }
    result
}

fn parse_file(lines: Vec<String>) -> (Vec<Range<usize>>, Vec<SourceDestMapping>) {
    let mut seeds = Vec::new();
    let mut mappings: Vec<SourceDestMapping> = Vec::new();

    let mut current_mapping = SourceDestMapping::new();

    for (idx, line) in lines.iter().enumerate() {
        if idx == 0 {
            seeds = parse_seeds_line_part_two(&line);
            continue;
        }

        if line.len() > 0 {
            if (line.as_bytes()[0] as char).is_ascii_alphabetic() {
                // mapping header line
                let header_vec: Vec<&str> = line.split_whitespace().collect::<Vec<&str>>()[0]
                    .split("-")
                    .collect();
                current_mapping.source = String::from(header_vec[0]);
                current_mapping.dest = String::from(header_vec[2]);
            } else {
                // collect ranges for current mapping
                let nums_vec = line
                    .split_whitespace()
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect::<Vec<usize>>();
                let source_range_start = nums_vec[1];
                let dest_range_start = nums_vec[0];
                let range_length = nums_vec[2];

                current_mapping.mapping_ranges.push(MappingRange::new(
                    source_range_start,
                    dest_range_start,
                    range_length,
                ));

                // if next line is empty, complete the mapping and reset
                if idx == lines.len() - 1 || lines[idx + 1].len() == 0 {
                    mappings.push(current_mapping);
                    current_mapping = SourceDestMapping::new();
                }
            }
        }
    }

    (seeds, mappings)
}

fn find_lowest_location_number(
    seeds: Vec<Range<usize>>,
    mut mappings: Vec<SourceDestMapping>,
) -> usize {
    mappings.reverse();

    let mut current_location = 46;
    loop {
        if dest_value_in_seeds(&current_location, &mappings, 0, &seeds) {
            return current_location;
        }
        current_location += 1;
    }
}

fn dest_value_in_seeds(
    dest: &usize,
    mappings: &Vec<SourceDestMapping>,
    cur_mapping_idx: usize,
    seeds: &Vec<Range<usize>>,
) -> bool {
    if cur_mapping_idx > mappings.len() - 1 {
        for range in seeds {
            if range.contains(&dest) {
                return true;
            }
        }
        return false;
    }
    let mapping = &mappings[cur_mapping_idx];
    let mut source_value = dest.clone();

    for m in &mapping.mapping_ranges {
        if m.dest_range.contains(&dest) {
            let offset = dest - m.dest_range.start;
            source_value = m.source_range.start + offset;
            continue;
        }
    }

    return dest_value_in_seeds(&source_value, mappings, cur_mapping_idx + 1, &seeds);
}  // 35


fn main() -> Result<()> {
    let file_path = std::env::args()
        .nth(1)
        .expect("Path to input file is required.");

    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    let (seeds, mappings) = parse_file(lines);

    println!(
        "Lowest location number: {}",
        find_lowest_location_number(seeds, mappings)
    );
    Ok(())
}
