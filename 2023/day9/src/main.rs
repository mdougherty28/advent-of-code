use std::io::Result;

use util;

fn count_uniques<T>(input_vec: &Vec<T>) -> usize
where
    T: std::cmp::Ord,
    Vec<T>: Clone,
{
    let mut r = input_vec.clone();
    r.sort();
    r.dedup();

    r.len()
}

fn calculate_next_val_for_line(line: String, get_previous: bool) -> isize {
    let mut vals: Vec<Vec<isize>> = Vec::new();
    vals.push(
        line.split_whitespace()
            .map(|l| l.parse::<isize>().unwrap())
            .collect(),
    );

    let mut last_row = vals.last().unwrap().clone();
    let mut next: Vec<isize> = Vec::new();

    while !(last_row[0] == 0 && count_uniques(&last_row) <= 1) {
        for i in 0..last_row.len() - 1 {
            next.push(last_row[i + 1] - last_row[i]);
        }

        vals.push(next.clone());

        last_row = vals.last().unwrap().clone();
        next = Vec::new();
    }

    vals.reverse();

    if !get_previous {
        vals[0].push(0);

        for i in 1..vals.len() {
            let bottom = &vals[i - 1][0].clone();
            let top = &vals[i][0].clone();
            vals[i].push(bottom + top);
        }

        return vals.last().unwrap().last().unwrap().clone();
    } else {
        let mut bottom = 0;
        let mut _top = vals[1][0];
        for i in 0..vals.len() - 1 {
            _top = vals[i + 1][0];
            bottom = _top - bottom;
        }

        return bottom;
    }
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines = util::read_file_to_vec_from_path(file_path)?;

    let part_one: isize = lines
        .clone()
        .into_iter()
        .map(|l| calculate_next_val_for_line(l, false))
        .sum();
    println!("Part one total: {:#?}", part_one);

    let part_two: isize = lines
        .into_iter()
        .map(|l| calculate_next_val_for_line(l, true))
        .sum();
    println!("Part two total: {:#?}", part_two);

    Ok(())
}
