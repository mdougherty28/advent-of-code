use std::collections::HashMap;
use std::fs::File;
use std::io::Result;
use std::io::{BufRead, BufReader};

use util;

type FieldMap = HashMap<String, [String; 2]>;

fn part_one(field: FieldMap, instruction: &Vec<u32>) {
    let mut step_count = 0;
    let mut current = "AAA".to_string();

    loop {
        for i in instruction {
            current = field.get(&current).unwrap()[i.clone() as usize].to_string();
            step_count += 1;
        }
        if current == "ZZZ" {
            break;
        }
    }

    println!("Part one total steps: {}", step_count);
}

fn part_two(field: FieldMap, instruction: &Vec<u32>) -> i64 {
    let paths = field
        .keys()
        .filter(|k| k.to_string().ends_with("A"))
        .map(|l| l.to_string())
        .collect::<Vec<String>>();

    let mut completed = Vec::new();
    for path in paths {
        let mut step_count = 0;
        let mut current = path.clone();
        loop {
            if current.chars().last().unwrap() == 'Z' {
                completed.push(step_count);
                break;
            }

            for i in instruction {
                current = field.get(&current).unwrap()[i.clone() as usize].to_string();
                step_count += 1;
            }
        }
    }

    lcm(completed.clone())
}

fn lcm(nums: Vec<i64>) -> i64 {
    if nums.len() == 1 {
        return nums[0];
    }
    let a = nums[0];
    let b = lcm(nums[1..].to_vec());
    a * b / gcd(a, b)
}

fn gcd(a: i64, b: i64) -> i64 {
    if b == 0 {
        return a;
    }
    gcd(b, a % b)
}

fn read_file_to_field(file_path: String) -> Result<(FieldMap, Vec<u32>)> {
    let file = File::open(file_path)?;
    let mut lines = BufReader::new(file).lines();

    let mut field = FieldMap::new();
    let instruction = lines
        .next()
        .unwrap()?
        .replace("L", "0")
        .replace("R", "1")
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect();

    for line in lines.skip(1) {
        let line = line?;

        let line: Vec<String> = line.clone().split(" = ").map(|l| l.to_string()).collect();
        let key = line[0].trim().to_string();
        let val = line[1].trim().to_string();
        let val = val
            .split(", ")
            .into_iter()
            .map(|v| v.replace("(", "").replace(")", "").to_string())
            .collect::<Vec<String>>();
        let val = [val[0].clone(), val[1].clone()];

        field.insert(key, val.clone());
    }

    Ok((field, instruction))
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let (field, instruction) = read_file_to_field(file_path)?;
    part_one(field.clone(), &instruction);

    println!("Part two total: {:#?}", part_two(field, &instruction));
    Ok(())
}
