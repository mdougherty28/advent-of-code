use std::fs::File;
use std::io::Result;
use std::io::{BufRead, BufReader};

#[derive(Debug, Clone)]
struct Card {
    winning_numbers: Vec<usize>,
    my_numbers: Vec<usize>,
    matches: Vec<usize>,
    copies: usize,
}

impl Card {
    fn parse_line_to_card(line: String) -> Self {
        let split_line: Vec<&str> = line.split(":").collect();

        let numbers = split_line[1].split("|").collect::<Vec<&str>>();

        Card {
            winning_numbers: parse_string_to_vec(numbers[0]),
            my_numbers: parse_string_to_vec(numbers[1]),
            matches: Vec::new(),
            copies: 1,
        }
    }

    fn find_matches(&mut self) {
        for num in &self.my_numbers {
            if self.winning_numbers.contains(&num) {
                self.matches.push(*num)
            }
        }
    }

    fn get_total_points(&self) -> i32 {
        let l = self.matches.len();
        if l == 0 {
            return 0;
        } else {
            return (2 as i32).pow(l as u32 - 1);
        };
    }
}

fn parse_string_to_vec(input_str: &str) -> Vec<usize> {
    input_str
        .trim()
        .split_whitespace()
        .map(|n| n.parse::<usize>().expect("Failed to parse numbers string."))
        .collect::<Vec<usize>>()
}

fn recursively_create_copies(mut cards: Vec<Card>, cur_idx: usize) -> Vec<Card> {
    if cur_idx == cards.len() - 1 {
        return cards;
    }

    if !cards[cur_idx].matches.is_empty() {
        for _ in 0..cards[cur_idx].copies {
            for i in 1..cards[cur_idx].matches.len() + 1 {
                cards[cur_idx + i].copies += 1;
            }
        }
    }

    recursively_create_copies(cards, cur_idx + 1)
}

fn main() -> Result<()> {
    let file_path = std::env::args()
        .nth(1)
        .expect("Path to input file is required.");

    let file = File::open(file_path)?;
    let reader = BufReader::new(file);

    let mut total = 0;

    let mut cards = Vec::new();

    for line in reader.lines() {
        let line = line?;
        let mut card = Card::parse_line_to_card(line);
        card.find_matches();
        total += card.get_total_points();

        cards.push(card);
    }

    let cards_after_copies = recursively_create_copies(cards, 0);
    let copied_total: usize = cards_after_copies.iter().map(|c| c.copies).sum();

    println!("Part one total: {}", total);
    println!("Part two total: {}", copied_total);

    Ok(())
}
