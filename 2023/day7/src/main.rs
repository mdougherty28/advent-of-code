use std::cmp::Ordering;
use std::{collections::HashMap, io::Result};
use util;

#[derive(Debug)]
struct Card {
    name: String,
    value: usize,
}

impl Card {
    fn new(card: char) -> Self {
        let value: usize = match card {
            'A' => 14,
            'K' => 13,
            'Q' => 12,
            'J' => 1,
            'T' => 10,
            _ => card.to_digit(10).unwrap() as usize,
        };

        Card {
            name: card.to_string(),
            value: value,
        }
    }
}

#[derive(Debug)]
struct Hand {
    card_counts: HashMap<String, usize>,
    cards: Vec<Card>,
    bid: usize,
    is_part_two: bool,
}

impl Hand {
    fn new(hand_string: String, bid: usize, is_part_two: bool) -> Self {
        let mut card_counts = HashMap::new();
        let mut cards = Vec::new();

        for c in hand_string.chars() {
            let card = Card::new(c);

            if card_counts.contains_key(&card.name) {
                *card_counts.get_mut(&card.name).unwrap() += 1;
            } else {
                card_counts.insert(card.name.clone(), 1);
            }
            cards.push(card);
        }

        Hand {
            card_counts: card_counts,
            cards: cards,
            bid: bid,
            is_part_two: is_part_two,
        }
    }

    fn get_value(&self) -> usize {
        let card_counts = if self.is_part_two {
            // replace J cards with the card that has the most
            let mut card_counts = self.card_counts.clone();

            let filtered_card_counts: HashMap<String, usize> = self
                .card_counts
                .clone()
                .into_iter()
                .filter(|c| c.0 != "J")
                .collect();

            let most_frequent_card = filtered_card_counts.iter().max_by(|a, b| a.1.cmp(&b.1));

            if let Some(most_frequent_card) = most_frequent_card {
                let mut num_js = 0;
                for card in &card_counts {
                    if card.0 == &"J".to_string() {
                        num_js += card.1;
                    }
                }

                *card_counts.get_mut(most_frequent_card.0).unwrap() += num_js;
                card_counts.remove("J");
            }

            card_counts
        } else {
            self.card_counts.clone()
        };

        let values: Vec<&usize> = card_counts.values().collect();

        return match card_counts.len() {
            1 => 6,
            2 => {
                if values.contains(&&4) {
                    5
                } else {
                    4
                }
            }
            3 => {
                if values.contains(&&3) {
                    3
                } else {
                    2
                }
            }
            4 => 1,
            5 => 0,

            _ => 0,
        };
    }

    fn cmp(&self, other: &Self) -> Ordering {
        let av = self.get_value();
        let bv = other.get_value();
        if av == bv {
            for (idx, c) in self.cards.iter().enumerate() {
                if c.value != other.cards[idx].value {
                    return c.value.cmp(&other.cards[idx].value);
                }
            }
        }
        return av.cmp(&bv);
    }
}

fn part_one(lines: Vec<String>) {
    let mut hands = Vec::new();

    for line in lines {
        let (hand, bid) = line.split_at(5);

        hands.push(Hand::new(
            hand.to_string(),
            bid.trim().parse::<usize>().unwrap(),
            false,
        ));
    }

    hands.sort_by(Hand::cmp);
    let total: usize = hands
        .iter()
        .enumerate()
        .map(|(idx, h)| h.bid * (idx + 1))
        .sum();

    println!("Part one: {:#?}", total);
}

fn part_two(lines: Vec<String>) {
    let mut hands = Vec::new();

    for line in lines {
        let (hand, bid) = line.split_at(5);

        hands.push(Hand::new(
            hand.to_string(),
            bid.trim().parse::<usize>().unwrap(),
            true,
        ));
    }

    hands.sort_by(Hand::cmp);
    let total: usize = hands
        .iter()
        .enumerate()
        .map(|(idx, h)| h.bid * (idx + 1))
        .sum();

    println!("Part two: {:#?}", total);
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines: Vec<String> = util::read_file_to_vec_from_path(file_path)?;
    part_one(lines.clone());
    part_two(lines);

    Ok(())
}
