use std::cmp;
use std::io::Result;
use util;

type Grid = Vec<Vec<char>>;

#[derive(Debug)]
struct Galaxy {
    x: isize,
    y: isize,
    id: usize,
}

impl Galaxy {
    fn new(x: isize, y: isize, id: usize) -> Self {
        Galaxy { x: x, y: y, id: id }
    }

    fn shortest_distance(
        &self,
        other: &Galaxy,
        empty_cols: &Vec<isize>,
        empty_rows: &Vec<isize>,
        expansion_factor: isize,
    ) -> isize {
        let col_range = cmp::min(other.x, self.x)..cmp::max(other.x, self.x + 1);
        let row_range = cmp::min(other.y, self.y)..cmp::max(other.y, self.y + 1);
        let col_mod = empty_cols
            .iter()
            .filter(|c| col_range.contains(c))
            .collect::<Vec<&isize>>()
            .len() as isize;
        let row_mod = empty_rows
            .iter()
            .filter(|r| row_range.contains(r))
            .collect::<Vec<&isize>>()
            .len() as isize;

        (other.x - self.x).abs()
            + (other.y - self.y).abs()
            + (expansion_factor * row_mod - row_mod)
            + (expansion_factor * col_mod - col_mod)
    }
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines = util::read_file_to_vec_from_path(file_path)?;

    let mut galaxies: Vec<Galaxy> = Vec::new();

    let grid: Grid = lines
        .into_iter()
        .zip(0i32..)
        .map(|(l, y)| {
            l.chars()
                .zip(0i32..)
                .map(|(c, x)| {
                    if c == '#' {
                        let id = if let Some(prev) = galaxies.last() {
                            prev.id + 1
                        } else {
                            1
                        };

                        galaxies.push(Galaxy::new(x as isize, y as isize, id));
                    }
                    return c;
                })
                .collect()
        })
        .collect();

    let mut empty_rows: Vec<isize> = (0..grid[0].len())
        .map(|v| isize::try_from(v).unwrap())
        .collect();
    let mut empty_cols: Vec<isize> = (0..grid.len())
        .map(|v| isize::try_from(v).unwrap())
        .collect();
    let mut pairs: Vec<(&Galaxy, &Galaxy)> = Vec::new();

    let galaxies_count = galaxies.len();
    for (idx, g) in galaxies.iter().enumerate() {
        pairs.extend(
            (idx + 1..galaxies_count)
                .into_iter()
                .map(|o| (&galaxies[idx], &galaxies[o])),
        );

        if empty_rows.contains(&g.y) {
            empty_rows.retain(|&y| y != g.y);
        }

        if empty_cols.contains(&g.x) {
            empty_cols.retain(|&x| x != g.x);
        }
    }

    let sum: isize = pairs
        .iter()
        .map(|p| p.0.shortest_distance(p.1, &empty_cols, &empty_rows, 2))
        .sum();
    println!("Part one: {:#?}", sum);

    let sum: isize = pairs
        .iter()
        .map(|p| {
            p.0.shortest_distance(p.1, &empty_cols, &empty_rows, 1000000)
        })
        .sum();
    println!("Part two: {:#?}", sum);

    Ok(())
}
