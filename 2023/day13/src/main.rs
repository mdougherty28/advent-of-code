use std::io::Result;
use util;

#[derive(Debug, Clone)]
struct Grid {
    coordinates: Vec<Vec<char>>,
    result: (usize, char),
}

impl Grid {
    fn new() -> Self {
        Grid {
            coordinates: Vec::new(),
            result: (0, 'N'),
        }
    }

    fn find_symmetry_axis(&mut self, mut buffer: usize) -> (usize, char) {
        for d in vec!['X', 'Y'] {
            let symmetry_check_range = if d == 'Y' {
                0..self.coordinates[0].len() - 1
            } else {
                0..self.coordinates.len() - 1
            };
            let mut axis = 0;
            for i in symmetry_check_range {
                axis = i;
                let mut all_mirrors_symmetrical = true;
                'y_loop: for (y, row) in self.coordinates.iter().enumerate() {
                    for (x, c) in row.iter().enumerate() {
                        {
                            if (d == 'Y' && x > i) || (d == 'X' && y > i) {
                                continue;
                            }

                            let result = point_has_symmetrical_pair(x, y, c, self, i, d);
                            if !result {
                                all_mirrors_symmetrical = false;
                                break 'y_loop;
                            }
                        }
                    }
                }

                if all_mirrors_symmetrical {
                    axis = i;
                    let multiplier = if d == 'Y' { 1 } else { 100 };
                    self.result = ((axis + 1) * multiplier, d);
                    return self.result;
                }
            }
        }

        (0, 'N')
    }
}

fn main() -> Result<()> {
    let file_path = util::get_file_path_from_args();
    let lines = util::read_file_to_vec_from_path(file_path)?;

    let mut grids = Vec::new();

    let mut grid = Grid::new();
    for l in lines.into_iter() {
        if l.len() == 0 {
            grids.push(grid);
            grid = Grid::new();
            continue;
        }
        grid.coordinates.push(l.chars().collect());
    }
    grids.push(grid);

    let sum: usize = grids.iter_mut().map(|g| g.find_symmetry_axis(0).0).sum();
    println!("Part one total: {:#?}", sum);

    // Part two still not correct??
    let mut grids2 = grids.clone();
    for mut g in grids2.iter_mut() {
        let old_result = g.result.clone();
        let mut result = find_smudged_symmetry(&g, false, old_result);

        // if result.0 == 0 {
        //     result = find_smudged_symmetry(&g, true, old_result);
        // }

        g.result = result;
    }

    let sum: usize = grids2.iter().map(|g| g.result.0).sum();
    println!("Part two total: {:#?}", sum);

    Ok(())
}

fn find_smudged_symmetry(
    g: &Grid,
    allow_original: bool,
    old_result: (usize, char),
) -> (usize, char) {
    for y in 0..g.coordinates.len() {
        for (x, c) in g.coordinates[y].iter().enumerate() {
            let mut new_g = g.clone();
            new_g.coordinates[y][x] = if c == &'#' { '.' } else { '#' };
            let (total, axis) = new_g.find_symmetry_axis(0);

            // if allow_original {
            //     if total == old_result.0 && axis == old_result.1 {
            //         return (total, axis);
            //     }
            // } else {
            if total > 0 {
                return (total, axis);
            }
            // }
        }
    }

    (0, 'N')
}

fn point_has_symmetrical_pair(
    x: usize,
    y: usize,
    c: &char,
    grid: &Grid,
    symmetry_index: usize,
    axis: char,
) -> bool {
    let width = grid.coordinates[0].len();
    let height = grid.coordinates.len();

    if axis == 'Y' {
        let distance_to_axis = symmetry_index - x;
        if symmetry_index + distance_to_axis + 1 >= width {
            return true;
        }
        if &grid.coordinates[y][symmetry_index + distance_to_axis + 1] == c {
            return true;
        }
    } else {
        let distance_to_axis = symmetry_index - y;
        if symmetry_index + distance_to_axis + 1 >= height {
            return true;
        }
        if &grid.coordinates[symmetry_index + distance_to_axis + 1][x] == c {
            return true;
        }
    }

    false
}
