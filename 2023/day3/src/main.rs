use std::io;
use std::io::Result;

#[derive(Debug, Clone, Copy)]
struct Coordinate {
    x: isize,
    y: isize,
    val: char,
    is_special_char: bool,
}

#[derive(Debug, Clone, Copy)]
struct Number {
    y: isize,
    start_x: isize,
    end_x: isize,
    val: isize,
}

#[derive(Debug)]
struct NumberSpecialCharMapping {
    special_char: Coordinate,
    number_mappings: Vec<Number>,
}

impl NumberSpecialCharMapping {
    fn get_gear_ratio(&self) -> isize {
        self.number_mappings
            .iter()
            .map(|m| m.val)
            .product::<isize>()
    }
}

fn special_char_check(c: char) -> bool {
    !c.is_numeric() && c != '.'
}

fn read_stdin_to_vec() -> Result<Vec<Vec<Coordinate>>> {
    let mut schematic = Vec::new();

    for (idx_y, line) in io::stdin().lines().enumerate() {
        let line = line?;
        let line_vec: Vec<Coordinate> = line
            .chars()
            .enumerate()
            .map(|(idx_x, c)| Coordinate {
                x: idx_x as isize,
                y: idx_y as isize,
                val: c,
                is_special_char: special_char_check(c),
            })
            .collect();
        schematic.push(line_vec);
    }

    Ok(schematic)
}

fn get_numbers_from_row_and_append(
    line: &Vec<Coordinate>,
    mut numbers: Vec<Number>,
) -> Vec<Number> {
    let mut num_vec = Vec::new();
    let mut currently_parsing_number = false;
    let mut start_x = -1;

    for (idx, coordinate) in line.into_iter().enumerate() {
        if coordinate.val.is_numeric() {
            if !currently_parsing_number {
                currently_parsing_number = true;
                start_x = coordinate.x;
            }

            num_vec.push(coordinate.val);

            if idx == line.len() - 1 || !line[idx + 1].val.is_numeric() {
                let end = coordinate.x;
                let num_str: String = num_vec.clone().into_iter().collect();
                let num = Number {
                    y: coordinate.y,
                    start_x: start_x,
                    end_x: end,
                    val: num_str.parse::<isize>().unwrap(),
                };
                numbers.push(num);
                num_vec.clear();
                currently_parsing_number = false;
            }
        }
    }

    numbers
}

fn find_special_chars(schematic: Vec<Vec<Coordinate>>) -> Vec<Coordinate> {
    schematic
        .into_iter()
        .flatten()
        .filter(|c| c.is_special_char)
        .collect::<Vec<Coordinate>>()
}

fn find_number_special_char_mappings(
    special_chars: Vec<Coordinate>,
    numbers: Vec<Number>,
) -> Vec<NumberSpecialCharMapping> {
    let mut results = Vec::new();
    for c in special_chars {
        let nm = NumberSpecialCharMapping {
            special_char: c.clone(),
            number_mappings: Vec::new(),
        };
        results.push(find_adjacent_special_chars(nm, &numbers))
    }

    results
}

fn find_adjacent_special_chars(
    mut number_char_mapping: NumberSpecialCharMapping,
    numbers: &Vec<Number>,
) -> NumberSpecialCharMapping {
    let c = number_char_mapping.special_char;
    for n in numbers {
        if (n.y - 1..n.y + 2).contains(&c.y) {
            if n.start_x - 1 == c.x || n.end_x + 1 == c.x || (n.start_x..n.end_x + 1).contains(&c.x)
            {
                number_char_mapping.number_mappings.push(n.clone());
                continue;
            }
        }
    }

    number_char_mapping
}

fn remove_empty_mappings(
    mappings_vec: Vec<NumberSpecialCharMapping>,
) -> Vec<NumberSpecialCharMapping> {
    mappings_vec
        .into_iter()
        .filter(|m| m.number_mappings.len() != 0)
        .collect()
}

fn print_part_numbers_total(mappings_vec: &Vec<NumberSpecialCharMapping>) {
    let part_numbers_total: isize = mappings_vec
        .iter()
        .map(|m| m.number_mappings.iter().map(|n| n.val).sum::<isize>())
        .sum();
    println!("Part numbers total: {}", part_numbers_total);
}

fn print_gear_ratios_total(mappings_vec: Vec<NumberSpecialCharMapping>) {
    let gears: Vec<NumberSpecialCharMapping> = mappings_vec
        .into_iter()
        .filter(|m| m.number_mappings.len() > 1)
        .collect();

    let gear_ratio_sum: isize = gears.iter().map(|g| g.get_gear_ratio()).sum();
    println!("Gear ratios total: {}", gear_ratio_sum);
}

fn main() -> Result<()> {
    let schematic = read_stdin_to_vec()?;
    let mut number_groups_vec = Vec::new();

    for row in &schematic {
        number_groups_vec = get_numbers_from_row_and_append(row, number_groups_vec);
    }

    let special_chars = find_special_chars(schematic);

    let number_special_char_mappings =
        find_number_special_char_mappings(special_chars, number_groups_vec);

    let number_special_char_mappings: Vec<NumberSpecialCharMapping> =
        remove_empty_mappings(number_special_char_mappings);

    print_part_numbers_total(&number_special_char_mappings);
    print_gear_ratios_total(number_special_char_mappings);

    Ok(())
}
