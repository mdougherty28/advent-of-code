use std::fs::File;
use std::io::Result;
use std::io::{BufRead, BufReader};

pub fn get_file_path_from_args() -> String {
    std::env::args()
        .nth(1)
        .expect("Path to input file is required.")
}

pub fn read_file_to_vec_from_path(file_path: String) -> Result<Vec<String>> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);

    Ok(reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>())
}
